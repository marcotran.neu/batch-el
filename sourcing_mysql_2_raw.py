import pyspark
from pyspark.sql import SparkSession

# .config("spark.jars", "mysql-connector-java-8.0.13") \
# Create SparkSession
#       .master("local[1]") \
spark = SparkSession.builder \
      .appName("Sourcing mysql") \
      .getOrCreate()

spark.conf.get("spark.sql.sources.partitionOverwriteMode")

spark.conf.set("spark.sql.sources.partitionOverwriteMode","dynamic")




df = spark.read \
    .format("jdbc") \
    .option("driver", "com.mysql.jdbc.Driver") \
    .option("url", "") \
    .option("dbtable", "groups") \
    .option("user", "user") \
    .option("password", "password") \
    .load()
database_name = ''
table_name = ''
df.writeTo(f"{database_name}.{table_name}") \
            .option("merge-schema", "true") \
            .option("check-ordering", "false").append()